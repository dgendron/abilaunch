Welcome to AbiLaunch
====================

This is a python module to easily launch abinit calculations. This modules
creates an easy interface with abipy.

Installation
------------

Download repository using git::

  $ git clone git@github.com:fgoudreault/abilaunch.git
  $ cd abilaunch

Execute setup script and install requirements::
  
  $ pip install -r requirements.txt
  $ pip install .

For a development installation (to modify the code without having to execute
the setup script each time). Use the `-e` flag for the `pip install` command
and install the (recommanded) optional packages::
 
  $ pip install -r requirements.txt
  $ pip install -r requirements_optional.txt
  $ pip install -e .

For the tests, the package abioutput is also needed. In another repository::

  $ git clone git@gitlab.com:fgoudreault/abioutput.git
  $ cd abioutput
  $ pip install -r requirements.txt
  $ pip install (-e) .
 
Tests
-----

Just use pytest in main repository for testing (pytest should be installed first)::

  $ pytest

Examples
--------

There are some examples in the "/examples" directory.

Contributors
------------

- Félix Antoine Goudreault
- Daniel Gendron
