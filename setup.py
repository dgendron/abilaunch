from setuptools import setup
import configparser
import os


# Default config path is in $HOME/.config
CONFIG_PATH = os.path.join(os.path.expanduser("~"), ".config", "abilaunch")


setup(name="abilaunch",
      description="Python package to ease launching calculation of abinit",
      python_requires=">=3.6")


# Setup config file
# create config directory if needed
dirname = os.path.dirname(CONFIG_PATH)
if not os.path.isdir(dirname):
    os.mkdir(dirname)
# create file if it does not exists
if not os.path.isfile(CONFIG_PATH):
    config = configparser.ConfigParser()
    # assume it is in the PATH variable
    config["DEFAULT"] = {"abinit_path": "abinit",
                         "default_pseudos_dir": "none",
                         "qsub": "False"}
    # write file
    with open(CONFIG_PATH, "w") as f:
        config.write(f)
    print("A configuration file has been created at %s. Edit it if needed." %
          CONFIG_PATH)
