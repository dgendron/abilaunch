#!/bin/bash

#PBS -N example

MPIRUN=""
EXECUTABLE=~/Workspace/abinit/build/master/vanilla/src/98_main/optic
INPUT=/Users/fgoudreault/Workspace/abilaunch/examples/optic_launcher/run/example.files
LOG=/Users/fgoudreault/Workspace/abilaunch/examples/optic_launcher/example.log
STDERR=/Users/fgoudreault/Workspace/abilaunch/examples/optic_launcher/example.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
