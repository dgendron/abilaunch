#!/bin/bash

#PBS -N example

MPIRUN=""
EXECUTABLE=/home/felix/Workspace/abinit/build/master/vanilla/src/98_main/abinit
INPUT=/home/felix/Workspace/abilaunch/examples/launcher/run/example.files
LOG=/home/felix/Workspace/abilaunch/examples/launcher/example.log
STDERR=/home/felix/Workspace/abilaunch/examples/launcher/example.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
