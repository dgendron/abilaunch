from abilaunch import Launcher


# tbase1_1 variables
variables = {"acell": (10, 10, 10),
             "ntypat": 1,
             "znucl": 1,
             "natom": 2,
             "typat": (1, 1),
             "xcart": ((-0.7, 0.0, 0.0), (0.7, 0.0, 0.0)),
             "ecut": 10.0,
             "kptopt": 0,
             "nkpt": 1,
             "nstep": 10,
             "toldfe": 1.0e-6,
             "diemac": 2.0,
             "optforces": 1}

l = Launcher(".",  # working directory
             "example",
             pseudos=["../pseudos/01h.pspgth"],
             abinit_variables=variables,
             abinit_command=("/home/felix/Workspace/abinit/build/master/"
                             "vanilla/src/98_main/abinit"),
             overwrite=True,
             run=True)
