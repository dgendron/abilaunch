#!/bin/bash

#PBS -N ecut10

MPIRUN=""
EXECUTABLE=/home/felix/Workspace/abinit/build/master/mpi/src/98_main/abinit
INPUT=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut10/run/ecut10.files
LOG=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut10/ecut10.log
STDERR=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut10/ecut10.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
