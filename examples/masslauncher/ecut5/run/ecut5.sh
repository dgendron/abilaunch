#!/bin/bash

#PBS -N ecut5

MPIRUN=""
EXECUTABLE=/home/felix/Workspace/abinit/build/master/mpi/src/98_main/abinit
INPUT=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut5/run/ecut5.files
LOG=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut5/ecut5.log
STDERR=/home/felix/Workspace/abilaunch/examples/masslauncher/ecut5/ecut5.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
