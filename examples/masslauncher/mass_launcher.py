from abilaunch import MassLauncher


tbase1_1_vars = {"acell": (10, 10, 10),
                 "ntypat": 1,
                 "znucl": 1,
                 "natom": 2,
                 "typat": (1, 1),
                 "xcart": ((-0.7, 0.0, 0.0), (0.7, 0.0, 0.0)),
                 # "ecut": 10.0,
                 "kptopt": 0,
                 "nkpt": 1,
                 "nstep": 10,
                 "toldfe": 1.0e-6,
                 "diemac": 2.0,
                 "optforces": 1}

launcher = MassLauncher(".",  # main working directory (root)
                        ["ecut5", "ecut10"],  # job name for each job
                        # common pseudos for each job
                        ["../pseudos/01h.pspgth"],
                        tbase1_1_vars,  # common input variables for each job
                        # specific variables for each job
                        [{"ecut": 5}, {"ecut": 10}],
                        # if needed, specific pseudos for each job
                        specific_pseudos=None,
                        run=False,  # last to arg are the same for each jobs
                        overwrite=True,
                        abinit_command=("/home/felix/Workspace/abinit/build/"
                                        "master/mpi/src/98_main/abinit"))
launcher.run()
