from abilaunch.writers import PBSFileWriter


path = "example.sh"
writer = PBSFileWriter(path,
                       "example.files",
                       "example.log",
                       "stderr",
                       jobname="example",
                       walltime="1:00:00",
                       nodes=1,
                       ppn=12,
                       mpirun_command="mpirun -npernode=12",
                       abinit_command="abinit",
                       modules=["MPI/Gnu/gcc4.9.2/openmpi/1.8.8"],
                       lines_before="cd $PBS_O_WORKDIR")
writer.write(overwrite=True)
