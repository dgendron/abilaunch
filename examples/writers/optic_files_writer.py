from abilaunch.writers import OpticFilesFileWriter


path = "optic_example.files"

writer = OpticFilesFileWriter(path,
                              "optic_example.in",
                              "optic_example.out",
                              "odat_optic_example",
                              output_data_dir=".")
writer.write(overwrite=True)
