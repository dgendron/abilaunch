from abilaunch.writers import FilesFileWriter


path = "example.files"

writer = FilesFileWriter(path,
                         "example.in",
                         "example.out",
                         "idat_example",
                         "odat_example",
                         "tmp_example",
                         ["pseudo1",
                          "pseudo2"],
                         input_data_dir=".",
                         output_data_dir=".",
                         tmp_data_dir=".")
writer.write(overwrite=True)
