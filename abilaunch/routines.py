# TOP LEVEL ROUTINES
import os


def check_abspath(path, join_before=None):
    """Check if a path is absolute. If not, make it absolute
    and if needed, join a path to it.

    Parameters
    ----------
    path : str
           The path to check if it is absolute.
    join_before : str, optional
                  If path is not absolute, join this path before
                  making it absolute.
    """
    # check if path is absolute, if not, join workdir and return absolute
    if not os.path.isabs(path):
        if join_before is not None:
            path = os.path.join(join_before, path)
        return full_abspath(path)
    return path


def full_abspath(path):
    """abspath + expanduser
    """
    return os.path.abspath(os.path.expanduser(path))


def return_list(arg):
    """Given the argument, returns a list.

    If it is a single string, returns a list of the string.
    If it is None, returns an empty list.
    """
    if arg is None:
        return []
    if isinstance(arg, str):
        return [arg]
    return arg
