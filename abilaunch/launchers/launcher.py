from abilaunch import USER_CONFIG
from ..approvers import InputApprover, InputParalApprover
from .bases import BaseLauncher
from ..writers import FilesFileWriter, InputFileWriter
from ..routines import check_abspath, full_abspath, return_list
import os


class Launcher(BaseLauncher):
    """Launcher class for an abinit calculation.
    """
    _loggername = "Launcher"
    _input_approver = InputApprover
    _input_paral_approver = InputParalApprover
    _input_writer = InputFileWriter
    _files_writer = FilesFileWriter

    def __init__(self, workdir, jobname, pseudos,
                 abinit_variables=None,
                 **kwargs):
        """Launcher class for an abinit calculation. It creates a calculation
        tree:

        workdir/ -> input file, log file, out file, stderr goes there
            input_data/ -> all simlinks goes there
            run/ -> files file, pbs file goes there
                output_data/ -> all output from abinit will go there

        Parameters
        ----------
        workdir : str
                  Path (can be relative to home) to working directory.
        pseudos : list, str
                  The list of path to the pseudos. If only one pseudo, this do
                  not need to be a list.
        jobname : str
                  Base name for the files and calculation.
        abinit_variables : dict
                           A dictionary containing all abinit variables
                           used in the input file.
                           Each key represents the name of a variable.
        kwargs : other arguments given to the base Launcher.
        """
        self.pseudos = return_list(pseudos)
        # create symlinks
        super().__init__(workdir, jobname, input_variables=abinit_variables,
                         **kwargs)

    def validate(self, *args, **kwargs):
        self.pseudos = self._validate_pseudos(self.pseudos,
                                              self.input_variables)
        super().validate(*args, **kwargs)

    def _validate_pseudos(self, pseudos, *args):
        pseudos = return_list(pseudos)
        real_pseudos = []
        for pseudo in pseudos:
            real_pseudos.append(self._check_pseudo_exists(pseudo))
        return real_pseudos

    def _check_pseudo_exists(self, pseudo):
        # check if a pseudo exists. If not, try to locate it under the default
        # pseudo directory
        # check locally
        local_path = check_abspath(pseudo, join_before=self.workdir)
        if os.path.isfile(local_path):
            return local_path
        # check in default pseudo dir
        if USER_CONFIG.default_pseudos_dir == "none":
            raise FileNotFoundError(f"Could not locate pseudo {pseudo}.")
        default = os.path.join(USER_CONFIG.default_pseudos_dir, pseudo)
        if os.path.isfile(default):
            return full_abspath(default)
        # if we are here, could not locate into default dir
        raise FileNotFoundError(f"Could not locate pseudo {pseudo}.")

    def _get_files_file_writer_args(self):
        args = [self.files_file_path,
                self.input_file_path,
                self.output_file_path,
                self.input_data_prefix,
                self.output_data_prefix,
                self.tmp_data_prefix,
                self.pseudos]
        kwargs = {"input_data_dir": self.input_data_dir,
                  "output_data_dir": self.output_data_dir,
                  "tmp_data_dir": self.output_data_dir}
        return args, kwargs
