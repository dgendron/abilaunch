from ..approvers import OpticInputApprover, OpticInputParalApprover
from .bases import BaseLauncher
from ..writers import OpticFilesFileWriter, OpticInputFileWriter


_LINK_VARS = ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile")


class OpticLauncher(BaseLauncher):
    """Launcher class for an optic calculation.
    """
    _loggername = "OpticLauncher"
    _input_writer = OpticInputFileWriter
    _files_writer = OpticFilesFileWriter
    _input_approver = OpticInputApprover
    _input_paral_approver = OpticInputParalApprover

    def __init__(self, *args, optic_variables=None,
                 optic_command=None, **kwargs):
        """Optic launcher init method. Creates a tree for the calculation
        similar to usual Launcher.

        workdir/
            input_data/ -> automatically links all the input files from
                           optic input variables.
            run/  -> files file, pbs file goes there.
                output_data/ -> all output from optic will go there.

        Parameters
        ----------
        optic_variables : dict
                          The dictionary of all optic input variables.
        overwrite : bool, optional
                    If set to true, previous files will be overwritten.
        optic_command : str
                        The path to the optic executable.
        kwargs : other arguments are passed to the PBS file.
        """
        super().__init__(*args, to_link=None,
                         input_variables=optic_variables,
                         abinit_command=optic_command, **kwargs)

    def _create_input_file_writer(self, *args, **kwargs):
        # link input files to input_data
        self.to_link = [self.input_variables[x] for x in _LINK_VARS]
        # reset optic variables with symlink target before creating input file
        for x in _LINK_VARS:
            iv = self.input_variables
            iv[x] = self._get_symlink_target(iv[x])
        super()._create_input_file_writer(*args, **kwargs)

    def _get_files_file_writer_args(self):
        args = [self.files_file_path,
                self.input_file_path,
                self.output_file_path,
                self.output_data_prefix]
        return args, {"output_data_dir": self.output_data_dir}
