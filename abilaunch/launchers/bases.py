from abilaunch import USER_CONFIG
from ..approvers import MPIApprover
from ..bases import BaseUtility
from ..routines import full_abspath, check_abspath, return_list
from ..writers import PBSFileWriter
import abc
import logging
import os
import subprocess


DATA_FILE_ENDINGS = ("DEN", "1WF", "WFQ", "WFK", "EIG", "KSS",
                     "GEO", "DDB", "DOS", "POT")


class cd:
    """Context manager for changing the current working directory.
    Taken from https://stackoverflow.com/a/13197763/6362595
    """
    def __init__(self, newPath):
        self.newPath = full_abspath(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


class BaseLauncher(BaseUtility):
    """Base class for launchers.
    """
    _input_writer = None
    _files_writer = None
    _input_approver = None
    _input_paral_approver = None
    _input_data_prefix = "idat"
    _output_data_prefix = "odat"
    _tmp_data_prefix = "tmp"

    def __init__(self, workdir, jobname, input_variables=None, run=False,
                 overwrite=False, to_link=None, **kwargs):
        """Base Launcher class init method.

        Created the directories and various files needed for the calculation.

        Parameters
        ----------
        workdir : str
                  Path to the working directory.
        jobname : str
                  The calculation jobname. All files will have this name.
        overwrite : bool, optional
                    If True, previous files will be overwritten.
        input_variables : dict
                          The dict of all the input variables.
        run : bool, optional
              If True, the job is run or is submitted depending if we are on
              a cluster or not.
        to_link : list, optional
                  List of files to link to the calculation (input data files).
        kwargs : other kwargs goes to the PBS file.
        """
        if input_variables is None:
            raise ValueError(f"No input variables given...")
        super().__init__(loglevel=kwargs.pop("loglevel", logging.INFO))
        self.workdir = full_abspath(workdir)
        self.jobname = jobname
        self._overwrite_enabled = overwrite
        self.input_variables = input_variables
        self.to_link = return_list(to_link)
        # before creating all files and dirs, approve input variables
        self.validate(self.input_variables, **kwargs)
        self.init_tree()
        self._create_writers(**kwargs)
        self.write()
        if run:
            self.run()

    def _create_input_data_links(self, jobname, links):
        for filename in links:
            path = check_abspath(filename, join_before=self.workdir)
            if not os.path.isfile(path):
                raise FileNotFoundError(f"Link not found: {path}.")
            # create link
            target = self._get_symlink_target(path)
            # check if target exists
            if os.path.exists(target):
                if self._overwrite_enabled:
                    os.remove(target)
                else:
                    raise FileExistsError(f"{target} already exists!")
            self._logger.info(f"Creating symlink: {target} -> {path}")
            os.symlink(path, target)

    def _get_symlink_target(self, path):
        # path is the source file to point to. return the link destination
        filename = os.path.basename(path)
        end = filename.split("_")[-1]
        # loop over all possible endings (instead of just if) to take into
        # account _1WFX files where X is a number.
        for ending in DATA_FILE_ENDINGS:
            if ending in end:
                break
        else:
            # executed only if no ending are in the end of the filename.
            raise NotImplementedError(f"file ending {end} not supported.")
        newname = self._input_data_prefix + "_" + self.jobname + "_" + end
        return os.path.join(self.input_data_dir, newname)

    def run(self):
        """Run or submit the calculation (if on a supercluster).
        """
        # check that batch file is there
        if not os.path.isfile(self.pbs_file_path):
            raise FileNotFoundError(f"Cannot run calculation without batch"
                                    f" file: {self.pbs_file_path}")
        if USER_CONFIG.qsub is True:
            self._logger.info(f"Submitting {self.jobname}.")
            command = ["qsub", self.pbs_file_path]
            runfunc = subprocess.run
        else:
            self._logger.info(f"Launching {self.jobname}.")
            command = ["bash", self.pbs_file_path]
            runfunc = subprocess.Popen
        with cd(self.rundir):
            runfunc(command)

    def validate(self, input_variables, **kwargs):
        # check the input variables
        paral_vars = {"nodes": kwargs.get("nodes", None),
                      "ppn": kwargs.get("ppn", None),
                      "mpirun_command": kwargs.get("mpirun_command", None)}
        # if all paralvars are None, use None instead
        useparal = None
        for v in paral_vars.values():
            if v is not None:
                useparal = paral_vars
                break
        lvl = self._logger.level
        inputapprover = self._input_approver(input_variables,
                                             loglevel=lvl)
        if not inputapprover.is_valid:
            raise ValueError(f"Input file errors: {inputapprover.errors}.")
        if useparal is not None:
            # approve mpi settings
            mpiapprover = MPIApprover(useparal,
                                      loglevel=lvl)
            if not mpiapprover.is_valid:
                raise ValueError(f"MPI settings not valid:"
                                 f" {mpiapprover.errors}.")
            # approve mpi settings with input file settings
            app = self._input_paral_approver.from_approvers(inputapprover,
                                                            mpiapprover,
                                                            loglevel=lvl)
            paral_approver = app
            if not paral_approver.is_valid:
                raise ValueError(f"Input file variables don't work with the"
                                 f" current MPI settings:"
                                 f" {paral_approver.errors}.")

    def write(self):
        """Write the files for the calculation.
        """
        self._logger.info(f"Writing input file at {self.input_file_path}")
        self.input_file_writer.write(overwrite=self._overwrite_enabled)
        self._logger.info(f"Writing files file at {self.files_file_path}")
        self.files_file_writer.write(overwrite=self._overwrite_enabled)
        self._logger.info(f"Writing batch file at {self.pbs_file_path}")
        self.pbs_file_writer.write(overwrite=self._overwrite_enabled)
        # create input data links
        self._create_input_data_links(self.jobname, self.to_link)

    def init_tree(self):
        # create directories for the calculation
        # create work dir
        self._mkdir(self.workdir)
        # create input data directory
        self.input_data_dir = os.path.join(self.workdir, "input_data")
        self._mkdir(self.input_data_dir)
        # create run data directory and output data directory
        self.rundir = os.path.join(self.workdir, "run")
        self._mkdir(self.rundir)
        self.output_data_dir = os.path.join(self.rundir, "output_data")
        self._mkdir(self.output_data_dir)

    def _mkdir(self, path):
        if os.path.isfile(path):
            raise FileExistsError(f"{path} is a file!")
        if os.path.isdir(path):
            self._logger.info(f"{path} already exists. Do not create.")
            return
        self._logger.info(f"Creating directory: {path}")
        os.mkdir(path)

    def _create_input_file_writer(self):
        loglevel = self._logger.level
        self.input_file_writer = self._input_writer(self.input_file_path,
                                                    self.input_variables,
                                                    loglevel=loglevel)

    def _create_files_file_writer(self):
        loglevel = self._logger.level
        files_args, files_kwargs = self._get_files_file_writer_args()
        self.files_file_writer = self._files_writer(*files_args,
                                                    loglevel=loglevel,
                                                    **files_kwargs)

    def _create_pbs_file_writer(self, **kwargs):
        loglevel = self._logger.level
        self.pbs_file_writer = PBSFileWriter(self.pbs_file_path,
                                             self.files_file_path,
                                             log_path=self.log_file_path,
                                             stderr_path=self.stderr_file_path,
                                             loglevel=loglevel,
                                             jobname=self.jobname,
                                             **kwargs)

    def _create_filenames(self):
        self.input_file_name = self.jobname + ".in"
        self.input_file_path = os.path.join(self.workdir, self.input_file_name)
        # output file name and path
        self.output_file_name = self.jobname + ".out"
        self.output_file_path = os.path.join(self.workdir,
                                             self.output_file_name)
        if os.path.isfile(self.output_file_path):
            self._logger.warning(f"There is already an output file named:"
                                 f" {self.output_file_path}")

        self.files_file_name = self.jobname + ".files"
        self.files_file_path = os.path.join(self.rundir, self.files_file_name)
        self.input_data_prefix = self._input_data_prefix + "_" + self.jobname
        self.output_data_prefix = self._output_data_prefix + "_" + self.jobname
        self.tmp_data_prefix = self._tmp_data_prefix + "_" + self.jobname

        self.pbs_file_name = self.jobname + ".sh"
        self.pbs_file_path = os.path.join(self.rundir, self.pbs_file_name)
        self.log_file_name = self.jobname + ".log"
        self.log_file_path = os.path.join(self.workdir, self.log_file_name)
        self.stderr_file_name = self.jobname + ".stderr"
        self.stderr_file_path = os.path.join(self.workdir,
                                             self.stderr_file_name)

    def _create_writers(self, **kwargs):
        # create file names and paths
        self._create_filenames()
        # files file writer
        self._create_files_file_writer()
        # input file writer
        self._create_input_file_writer()
        # batch file writer
        self._create_pbs_file_writer(**kwargs)

    @abc.abstractmethod
    def _get_files_file_writer_args(self):
        pass
