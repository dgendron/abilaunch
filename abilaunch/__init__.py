from .config import ConfigFileParser
USER_CONFIG = ConfigFileParser()
from .launchers import Launcher, MassLauncher, OpticLauncher
