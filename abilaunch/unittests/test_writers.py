from abilaunch.writers import (FilesFileWriter, InputFileWriter, PBSFileWriter,
                               OpticFilesFileWriter, OpticInputFileWriter)
from abioutput.parsers import (FilesFileParser, InputFileParser,
                               OpticFilesFileParser, OpticInputFileParser,
                               PBSFileParser)
import os
import unittest
import tempfile


# tbase1_1 input variables
input_vars = {"acell": [10, 10, 10],
              "ntypat": 1,
              "znucl": 1,
              "natom": 2,
              "typat": [1, 1],
              "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
              "ecut": 10.0,
              "kptopt": 0,
              "nkpt": 1,
              "nstep": 10,
              "toldfe": 1.0e-6,
              "diemac": 2.0,
              "optforces": 1}


class BaseWritersTest:
    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_error_prevent_overwrite(self):
        # check that trying to overwrite it raises an error
        with self.assertRaises(FileExistsError):
            self.writer.write()


class TestPBSFileWriter(BaseWritersTest, unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test.sh")
        self.files_path = "test.files"
        self.log_path = "test.log"
        self.stderr_path = "stderr"
        self.walltime = "01:00:00"
        self.jobname = "test"
        self.nodes = 1
        self.ppn = 12
        self.mpirun_command = "mpirun -npernode 12"
        self.abinit_command = "abinit"
        self.modules = ["MPI/Gnu/gcc4.9.2/openmpi/1.8.8"]
        self.lines_before = ["cd $PBS_O_WORKDIR"]
        self.writer = PBSFileWriter(self.path,
                                    self.files_path,
                                    log_path=self.log_path,
                                    stderr_path=self.stderr_path,
                                    jobname=self.jobname,
                                    walltime=self.walltime,
                                    nodes=self.nodes,
                                    ppn=self.ppn,
                                    mpirun_command=self.mpirun_command,
                                    abinit_command=self.abinit_command,
                                    modules=self.modules,
                                    lines_before=self.lines_before)
        self.writer.write()

    def test_pbs_file_writer(self):
        # check that file exists
        self.assertTrue(os.path.isfile(self.writer.path))
        # check that it has written all it had to
        parser = PBSFileParser(self.path)
        self.assertEqual(parser["files_file_path"],
                         self.writer.files_file_path)
        self.assertEqual(parser["log_path"], self.writer.log_path)
        self.assertEqual(parser["stderr_path"], self.writer.stderr_path)
        self.assertEqual(parser["walltime"], self.writer.walltime)
        self.assertEqual(parser["nodes"], self.writer.nodes)
        self.assertEqual(parser["jobname"], self.writer.jobname)
        self.assertEqual(parser["ppn"], self.writer.ppn)
        self.assertEqual(parser["mpirun_command"], self.writer.mpirun_command)
        self.assertEqual(parser["abinit_command"], self.writer.abinit_command)
        self.assertListEqual(parser["lines_before"], self.writer.lines_before)
        self.assertListEqual(parser["lines_after"], self.writer.lines_after)
        self.assertListEqual(parser["modules"], self.writer.modules)


class TestInputFileWriter(BaseWritersTest, unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test.in")
        self.writer = InputFileWriter(self.path, input_vars)
        self.writer.write()

    def test_input_file_writer(self):
        # check that file exists
        self.assertTrue(os.path.isfile(self.path))
        # check that it has written everything it had to
        parser = InputFileParser(self.path)
        self.assertDictEqual(parser.input_variables, input_vars)


class TestOpticInputFileWriter(BaseWritersTest, unittest.TestCase):
    _variables = {"ddkfile_1": "toptic_1o_DS4_1WF7",
                  "ddkfile_2": "toptic_1o_DS5_1WF8",
                  "ddkfile_3": "toptoc_1o_DS6_1WF9",
                  "wfkfile": "toptic_1o_DS3_WFK",
                  "broadening": 0.002,
                  "domega": 0.0003,
                  "maxomega": 0.3,
                  "scissor": 0.000,
                  "tolerance": 0.002,
                  "num_lin_comp": 1,
                  "lin_comp": 11,
                  "num_nonlin_comp": 2,
                  "nonlin_comp": [123, 222],
                  "num_linel_comp": 0,
                  "num_nonlin2_comp": 0}

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "optic_test.in")
        self.writer = OpticInputFileWriter(self.path, self._variables)
        self.writer.write()

    def test_optic_input_file_writer(self):
        # check that file exists
        self.assertTrue(os.path.isfile(self.path))
        # check that the writer wrote what he had to
        parser = OpticInputFileParser(self.path)
        self.assertDictEqual(parser.input_variables, self._variables)


class TestOpticFilesFileWriter(BaseWritersTest, unittest.TestCase):
    _input = "optic_test.in"
    _output = "optic_test.out"
    _output_prefix = "odat_optic_test"
    _odd = "."

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "optic_test.files")
        self.writer = OpticFilesFileWriter(self.path,
                                           self._input,
                                           self._output,
                                           self._output_prefix,
                                           output_data_dir=self._odd)
        self.writer.write()

    def test_optic_files_file_writer(self):
        # check that file exists
        self.assertTrue(os.path.isfile(self.path))
        # check it had written everything it had to
        parser = OpticFilesFileParser(self.path)
        self.assertEqual(self.writer.input_file_path,
                         parser["input_path"])
        self.assertEqual(self.writer.output_file_path,
                         parser["output_path"])
        self.assertEqual(self.writer.output_data_prefix,
                         parser["output_data_prefix"])


class TestFilesFileWriter(BaseWritersTest, unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test.files")
        self.test_in = "test.in"
        self.test_out = "test.out"
        self.test_input_prefix = "idat_test"
        self.test_output_prefix = "odat_test"
        self.test_tmp_prefix = "tmp_test"
        self.test_pseudos = ["pseudo1", "pseudo2"]
        self.writer = FilesFileWriter(self.path,
                                      self.test_in,
                                      self.test_out,
                                      self.test_input_prefix,
                                      self.test_output_prefix,
                                      self.test_tmp_prefix,
                                      self.test_pseudos)
        self.writer.write()

    def test_files_file_writer(self):
        # check file exists
        self.assertTrue(os.path.isfile(self.path))
        # check that it has written everything it had to
        parser = FilesFileParser(self.path)
        self.assertEqual(parser.data["input_path"],
                         os.path.join(self.tempdir.name, self.test_in))
        self.assertEqual(parser.data["output_path"],
                         os.path.join(self.tempdir.name, self.test_out))
        self.assertEqual(parser.data["input_prefix"],
                         os.path.join(self.tempdir.name,
                                      self.test_input_prefix))
        self.assertEqual(parser.data["output_prefix"],
                         os.path.join(self.tempdir.name,
                                      self.test_output_prefix))
        self.assertEqual(parser.data["tmp_prefix"],
                         os.path.join(self.tempdir.name, self.test_tmp_prefix))
        self.assertListEqual(parser.data["pseudos"],
                             [os.path.join(self.tempdir.name, x)
                              for x in self.test_pseudos])
