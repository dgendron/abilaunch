import unittest
from abilaunch.approvers import (InputApprover, InputParalApprover,
                                 MPIApprover)
from abilaunch.approvers.input_approver import (MANDATORY_VARS,
                                                TOLERANCES_VARS,
                                                ATOMIC_POSITION_VARS)
from .test_launcher import tbase1_1_vars


BASIC_MPI_VARIABLES = {"ppn": 12,
                       "mpirun_command": "mpirun -npernode 12",
                       "nodes": "3:m48G"}


class TestBaseApprover(unittest.TestCase):
    _approver = None

    def test_no_dict_raise(self):
        with self.assertRaises(TypeError):
            self._approver(1)


class TestInputApprover(TestBaseApprover):
    _approver = InputApprover

    def setUp(self):
        self.variables = tbase1_1_vars.copy()

    def test_input_approver(self):
        # bare tbase 1_1 variables should raise no error
        approver = InputApprover(self.variables)
        self.assertEqual(len(approver.errors), 0)
        self.assertTrue(approver.is_valid)

    def test_tolwfr_if_nscf(self):
        self.variables["iscf"] = -2
        self.variables.pop("toldfe")
        approver = InputApprover(self.variables)
        self.assertFalse(approver.is_valid)
        self.assertEqual(len(approver.errors), 1)

    def test_missing_mandatory_variable(self):
        for var in MANDATORY_VARS:
            inputs = self.variables.copy()
            inputs.pop(var)
            approver = InputApprover(inputs)
            self.assertFalse(approver.is_valid)
            self.assertEqual(len(approver.errors), 1)
            del approver

    def test_one_tolerances(self):
        for tolerance in TOLERANCES_VARS:
            if tolerance == "toldfe":
                continue
            inputs = self.variables.copy()
            inputs[tolerance] = 10e-6
            approver = InputApprover(inputs)
            self.assertFalse(approver.is_valid)
            self.assertEqual(len(approver.errors), 1)

        # check at least one tolerance is there
        self.variables.pop("toldfe")
        approver = InputApprover(inputs)
        self.assertFalse(approver.is_valid)
        self.assertEqual(len(approver.errors), 1)

    def test_one_atomic_position(self):
        for atomic in ATOMIC_POSITION_VARS:
            if atomic == "xcart":
                continue
            inputs = self.variables.copy()
            inputs[atomic] = "atomic position"
            approver = InputApprover(inputs)
            self.assertFalse(approver.is_valid)
            self.assertEqual(len(approver.errors), 1)

        # check at least one tolerance is there
        self.variables.pop("xcart")
        approver = InputApprover(inputs)
        self.assertFalse(approver.is_valid)
        self.assertEqual(len(approver.errors), 1)


class TestInputParalApprover(TestBaseApprover):
    _approver = InputParalApprover

    def setUp(self):
        self.mpi_variables = BASIC_MPI_VARIABLES.copy()
        self.abinit_variables = tbase1_1_vars.copy()

    def test_approver_ok(self):
        inputs = self.abinit_variables.copy()
        inputs["autoparal"] = 1
        # check that it works when directly giving inputs
        approver = InputParalApprover(inputs, self.mpi_variables)
        self.assertTrue(approver.is_valid)
        self.assertEqual(len(approver.errors), 0)
        del approver
        # check that it works when giving corresponding approvers
        inputapprover = InputApprover(inputs)
        mpiapprover = MPIApprover(self.mpi_variables)
        approver = InputParalApprover.from_approvers(inputapprover,
                                                     mpiapprover)
        self.assertTrue(approver.is_valid)
        self.assertEqual(len(approver.errors), 0)
        del approver

    def test_raise_init_from_approvers(self):
        inputapprover = InputApprover(self.abinit_variables)
        mpiapprover = MPIApprover(self.mpi_variables)
        with self.assertRaises(TypeError):
            self._approver.from_approvers(1, mpiapprover)
        with self.assertRaises(TypeError):
            self._approver.from_approvers(inputapprover, 1)
        with self.assertRaises(TypeError):
            self._approver.from_approvers(1, 1)

    def test_fail_if_more_proc_asked_than_avail(self):
        self.abinit_variables["npkpt"] = 100
        approver = InputParalApprover(self.abinit_variables,
                                      self.mpi_variables)
        self.assertFalse(approver.is_valid)
        self.assertEqual(len(approver.errors), 1)


class TestMPIApprover(TestBaseApprover):
    _approver = MPIApprover

    def setUp(self):
        self.variables = BASIC_MPI_VARIABLES.copy()

    def test_approver_ok(self):
        approver = MPIApprover(self.variables)
        self.assertTrue(approver.is_valid)
        self.assertEqual(len(approver.errors), 0)

    def test_ask_too_much_npernode_for_ppn(self):
        mpi = self.variables.copy()
        ppn = mpi["ppn"]
        mpi["mpirun_command"] = f"mpirun -npernode {ppn + 1}"
        approver = MPIApprover(mpi)
        self.assertFalse(approver.is_valid)
        self.assertEqual(len(approver.errors), 1)
