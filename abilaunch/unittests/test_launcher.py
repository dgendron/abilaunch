import os
import tempfile
import unittest
from abilaunch import Launcher, OpticLauncher
from abioutput.parsers import OpticInputFileParser
from abilaunch.routines import full_abspath


here = os.path.dirname(os.path.abspath(__file__))
Hpseudo = os.path.join(here, "../../examples/pseudos/01h.pspgth")
tbase1_1_vars = {"acell": (10, 10, 10),
                 "ntypat": 1,
                 "znucl": 1,
                 "natom": 2,
                 "typat": (1, 1),
                 "xcart": ((-0.7, 0.0, 0.0), (0.7, 0.0, 0.0)),
                 "ecut": 10.0,
                 "kptopt": 0,
                 "nkpt": 1,
                 "nstep": 10,
                 "toldfe": 1.0e-6,
                 "diemac": 2.0,
                 "optforces": 1}

ddk1 = os.path.join(here, "../../examples/example_data/odat_example_1WF7")
ddk2 = os.path.join(here, "../../examples/example_data/odat_example_1WF8")
ddk3 = os.path.join(here, "../../examples/example_data/odat_example_1WF9")
wfk = os.path.join(here, "../../examples/example_data/odat_example_WFK")
toptic1_2_vars = {"ddkfile_1": ddk1,
                  "ddkfile_2": ddk2,
                  "ddkfile_3": ddk3,
                  "wfkfile": wfk,
                  "broadening": 0.002,
                  "maxomega": 0.3,
                  "domega": 0.0003,
                  "tolerance": 0.002,
                  "num_lin_comp": 1,
                  "lin_comp": 11,
                  "num_nonlin_comp": 2,
                  "nonlin_comp": [123, 222],
                  "num_linel_comp": 0,
                  "num_nonlin2_comp": 0}


class BaseLauncherTest:
    _jobname = None
    _launcher = None
    _var_kwarg_name = None
    _args = []
    _vars = None

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.workdir = os.path.join(self.tempdir.name, self._jobname)
        self._kwargs = {self._var_kwarg_name: self._vars.copy(),
                        "run": False}

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_launcher_from_args(self):
        self.launcher = self._launcher(self.workdir,
                                       self._jobname,
                                       *self._args,
                                       **self._kwargs)
        # check that input file is created in good dir
        self.assertTrue(os.path.isdir(self.launcher.workdir))
        self.assertTrue(os.path.isfile(self.launcher.input_file_path))
        self.assertTrue(os.path.isfile(self.launcher.pbs_file_path))
        self.assertTrue(os.path.isfile(self.launcher.files_file_path))
        self.assertTrue(os.path.isdir(self.launcher.input_data_dir))
        self.assertTrue(os.path.isdir(self.launcher.rundir))
        self.assertTrue(os.path.isdir(self.launcher.output_data_dir))

    def test_raise_when_no_variables(self):
        with self.assertRaises(ValueError):
            newkwargs = self._kwargs.copy()
            newkwargs.pop(self._var_kwarg_name)
            self.launcher = self._launcher(self.workdir,
                                           self._jobname,
                                           *self._args,
                                           **newkwargs)


class TestOpticLauncher(BaseLauncherTest, unittest.TestCase):
    _launcher = OpticLauncher
    _jobname = "test_optic_launcher"
    _vars = toptic1_2_vars
    _var_kwarg_name = "optic_variables"

    def test_input_links(self):
        self.launcher = self._launcher(self.workdir,
                                       self._jobname,
                                       **self._kwargs)
        links = os.listdir(self.launcher.input_data_dir)
        dests = []
        for link in links:
            # check link exists
            path = os.path.join(self.launcher.input_data_dir, link)
            dests.append(os.path.abspath(os.readlink(path)))
            self.assertTrue(os.path.islink(path))
        # check that all links are good in input file
        parser = OpticInputFileParser(self.launcher.input_file_path)
        for name in ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"):
            self.assertIn(os.path.basename(parser[name]), links)
            # check that linked is good
            self.assertIn(full_abspath(toptic1_2_vars[name]), dests)


class TestLauncher(BaseLauncherTest, unittest.TestCase):
    _launcher = Launcher
    _jobname = "test_launcher"
    _args = [Hpseudo]
    _var_kwarg_name = "abinit_variables"
    _vars = tbase1_1_vars

    def test_raise_pseudos_not_exists(self):
        with self.assertRaises(FileNotFoundError):
            # create new non existant pseudo
            Hpseudo2 = os.path.expanduser("~/pseudo")
            self.launcher = self._launcher(self.workdir,
                                           self._jobname,
                                           [Hpseudo2],
                                           **self._kwargs)

    def test_linking(self):
        # create file to link
        tempdir2 = tempfile.TemporaryDirectory()
        path = os.path.join(tempdir2.name, "odat_WFK")
        with open(path, "a") as f:
            f.write("test")
        self.launcher = self._launcher(self.workdir,
                                       self._jobname,
                                       *self._args,
                                       to_link=path,
                                       **self._kwargs)
        link = os.path.join(self.workdir, "input_data",
                            "idat_" + self._jobname + "_WFK")
        self.assertTrue(os.path.islink(link))
        tempdir2.cleanup()
        del tempdir2

    def test_link_raise(self):
        with self.assertRaises(FileNotFoundError):
            path = os.path.join(self.workdir, "odat_WFK")
            self.launcher = self._launcher(self.workdir,
                                           self._jobname,
                                           *self._args,
                                           to_link=path,
                                           **self._kwargs)
