# ABINIT INPUT VARIABLES DATABASE
# ORGANIZED BY BLOCKS OF VARIABLES

# inspired from:
# https://github.com/abinit/abipy/blob/v0.2.0/abipy/htc/inputfile.py

ALL_ABINIT_VARIABLES = {
        "basis set": ("ecut", "ecutsm"),
        "bands": ("nband", "nbdbuf", "occopt"),
        "checks": ("chksymbreak", ),
        "fft grid": ("ngfft", ),
        "k-point grid": ("kptbounds", "kptopt", "kptrlatt",
                         "ngkpt", "ndivk", "ndivsm",
                         "nkpt", "nshiftk", "shiftk"),
        "models": ("ixc", "nspden", "nspinor", "nsppol"),
        "parallelization": ("autoparal", "npband", "npfft", "nphf",
                            "npimage", "npkpt", "nppert",
                            "npspinor", "paral_kgb", "paral_rf"),
        "printing / reading": ("iomode", "irdden", "irdwfk",
                               "prteig", "prtvol"),
        "response function": ("nqpt", "qpt", "rfatpol", "rfdir", "rfelfd",
                              "rfphon"),
        "scf procedure": ("diemac", "iscf", "nstep", "optforces",
                          "toldfe", "toldff", "tolvrs", "tolwfk", "tolwfr"),
        "structural optimization": ("dilatmx", "ionmov", "ntime", "optcell",
                                    "tolmxf"),
        "unit cell": ("acell", "natom", "ntypat", "rprim",
                      "typat", "xcart", "xred", "znucl"),
        "van der waals": ("vdw_df_threshold", "vdw_xc"),
        }


ALL_OPTIC_VARIABLES = {
        "files": ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"),
        "parameters": ("broadening", "domega", "maxomega", "scissor",
                       "tolerance"),
        "computations": ("num_lin_comp", "lin_comp", "num_nonlin_comp",
                         "nonlin_comp", "num_linel_comp", "num_nonlin2_comp"),
        }
