from .bases import BaseInputFileWriter
from .input_variables import OpticInputVariable
from .variables_db import ALL_OPTIC_VARIABLES


class OpticInputFileWriter(BaseInputFileWriter):
    """Class that can write an optic input file.
    """
    _loggername = "OpticInputFileWriter"
    _file_suffix = ".in"
    _variable_class = OpticInputVariable

    def __init__(self, *args, **kwargs):
        """Optic input file writer init method.
        """
        super().__init__(*args, **kwargs)

    def _do_write(self):
        # start with files
        lines = []
        self._append_var_block(lines, "files")
        # parameters
        self._append_var_block(lines, "parameters")
        # computations
        self._append_var_block(lines, "computations", end_newline=False)
        self._write_lines(lines)

    def _append_var_block(self, lines, section, end_newline=True):
        lines.append(f"&{section.upper()}\n")
        for name in ALL_OPTIC_VARIABLES[section]:
            if name in self.input_variables:
                lines.append(str(self[name]))
        if end_newline:
            lines.append("/\n")
        else:
            lines.append("/")
