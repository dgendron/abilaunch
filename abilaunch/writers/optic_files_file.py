from .bases import BaseWriter
from ..routines import check_abspath
import os


class OpticFilesFileWriter(BaseWriter):
    """Class that writes an Optic files file.
    """
    _loggername = "OpticFilesFileWriter"
    _file_suffix = ".files"

    def __init__(self, path,
                 input_file_path,
                 output_file_path,
                 output_data_prefix,
                 output_data_dir=".", **kwargs):
        """Optic files file writer init method.

        Parameters
        ----------
        path : str
               The path where the files file will be written.
        input_file_path : str
                          The file path to the optic input file.
        output_file_path : str
                           The file path to the optic output file.
        output_data_prefix : str
                             The prefix for the optic output data files.
        output_data_dir : str, optional
                          The output data directory.
        """
        super().__init__(path, **kwargs)
        self.input_file_path = check_abspath(input_file_path,
                                             join_before=self.workdir)
        self.output_file_path = check_abspath(output_file_path,
                                              join_before=self.workdir)
        odp = os.path.join(output_data_dir, output_data_prefix)
        self.output_data_prefix = check_abspath(odp,
                                                join_before=self.workdir)

    def _do_write(self):
        lines = []
        # first line is input file
        lines.append(self.input_file_path + "\n")
        # second line is output file
        lines.append(self.output_file_path + "\n")
        # third line is output data prefix
        lines.append(self.output_data_prefix)
        self._write_lines(lines)
