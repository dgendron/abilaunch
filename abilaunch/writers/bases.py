from ..bases import BaseUtility
from ..routines import full_abspath
import abc
import os


class BaseWriter(BaseUtility):
    """Base class for writers.
    """
    _file_suffix = None

    def __init__(self, path, **kwargs):
        """Base writer init method.

        Parameters
        ----------
        path : str
               The path where the file will be written by the writer.
        """
        super().__init__(**kwargs)
        self.workdir = full_abspath(os.path.dirname(path))
        self.filename = os.path.basename(path)

        if not self.filename.endswith(self._file_suffix):
            self.filename += self._file_suffix
        self.path = os.path.join(self.workdir, self.filename)

    def write(self, overwrite=False):
        """Write the file.

        Parameters
        ----------
        overwrite: bool, optional
                   If False, an error is raised when attempting to write a
                   file that already exists under the same name.
        """
        if os.path.exists(self.path):
            if not overwrite:
                # raise the error
                raise FileExistsError(f"{self.path} already exists.")
            # else, erase file and rewrite
            os.remove(self.path)
        self._do_write()

    def _write_lines(self, lines):
        with open(self.path, "w") as f:
            for line in lines:
                f.write(line)

    @abc.abstractmethod
    def _do_write(self):
        pass


class BaseInputFileWriter(BaseWriter):
    """Input File Writer base class.
    """
    _variable_class = None

    def __init__(self, path, input_variables, **kwargs):
        """Base Input file writer init method.

        Parameters
        ----------
        path : str
               The path to the input file (where it will be written).
        input_variables : dict
                          The dictionary of input variables.
        """
        super().__init__(path, **kwargs)
        level = self._logger.level
        self.input_variables = {k: self._variable_class(k, v, loglevel=level)
                                for k, v in input_variables.items()}

    def __getitem__(self, value):
        return self.input_variables[value]


class BaseInputVariable(BaseUtility):
    """Base class for input variables.
    """

    def __init__(self, name, value, **kwargs):
        """Base input variable init method.
        """
        super().__init__(**kwargs)
        self.name = name
        self.value = value
        self.block = self._get_block()

    @abc.abstractmethod
    def _get_block(self):
        pass

    def __repr__(self):
        return str(self)
