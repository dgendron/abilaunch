from .bases import BaseWriter
from ..routines import check_abspath, return_list
import os


class FilesFileWriter(BaseWriter):
    """Class that represents an Abinit files file and writes it."""
    _loggername = "FilesFileWriter"
    _file_suffix = ".files"

    def __init__(self, path,
                 input_file_path,
                 output_file_path,
                 input_data_prefix,
                 output_data_prefix,
                 tmp_data_prefix,
                 pseudos,
                 input_data_dir=".",
                 output_data_dir=".",
                 tmp_data_dir=".",
                 **kwargs):
        """Files file writer init method.

        Parameters
        ----------
        path : str
               The path where the files file will be written.
        input_file_path : str
                          The file path to the abinit input file.
        output_file_path : str
                           The path to the abinit output file.
        input_data_prefix : str
                            The prefix for the input data files.
        output_data_prefix : str
                             The prefix for the output data files.
        tmp_data_prefix : str
                          The prefix for the temporary data files.
        pseudos : str, list
                  The list of pseudopotential files.
        input_data_dir : str, optional
                         The input data directory.
        output_data_dir : str, optional
                          The output data directory.
        tmp_data_dir : str, optional
                       The temporary data directory.
        """
        super().__init__(path, **kwargs)
        self.input_file_path = check_abspath(input_file_path,
                                             join_before=self.workdir)
        self.output_file_path = check_abspath(output_file_path,
                                              join_before=self.workdir)
        idp = os.path.join(input_data_dir, input_data_prefix)
        self.input_data_prefix = check_abspath(idp, join_before=self.workdir)
        odp = os.path.join(output_data_dir, output_data_prefix)
        self.output_data_prefix = check_abspath(odp, join_before=self.workdir)
        tdp = os.path.join(tmp_data_dir, tmp_data_prefix)
        self.tmp_data_prefix = check_abspath(tdp, join_before=self.workdir)
        pseudos = return_list(pseudos)
        self.pseudos = [check_abspath(x, join_before=self.workdir)
                        for x in pseudos]

    def _do_write(self):
        lines = []
        # first is input file
        lines.append(self.input_file_path + "\n")
        # second is output file
        lines.append(self.output_file_path + "\n")
        # third is input data prefix
        lines.append(self.input_data_prefix + "\n")
        # fourth is output data prefix
        lines.append(self.output_data_prefix + "\n")
        # fifth is tmp data prefix
        lines.append(self.tmp_data_prefix + "\n")
        # finally it is the pseudos
        lines += [x + "\n" for x in self.pseudos]
        self._write_lines(lines)
