import numpy as np
from .bases import BaseInputVariable
from .variables_db import ALL_ABINIT_VARIABLES, ALL_OPTIC_VARIABLES


_SPECIAL_VARIABLE_ENDING = (":", "+", "*", "?")
_SPACES = " " * 2


class AbinitInputVariable(BaseInputVariable):
    """Class that represents an abinit input variable.
    """
    _loggername = "AbinitInputVariable"

    def __init__(self, *args, **kwargs):
        """Abinit input variable init method.
        """
        self._basename = None
        super().__init__(*args, **kwargs)

    @property
    def basename(self):
        if self._basename is not None:
            return self._basename
        clean = self.name.strip("".join(_SPECIAL_VARIABLE_ENDING))
        while clean[-1].isdigit():
            # remove datasets labels
            clean = clean[:-1]
        self._basename = clean
        return clean

    def _get_block(self):
        # get the block name in which this variable lives
        for block, variables in ALL_ABINIT_VARIABLES.items():
            if self.basename in variables:
                return block
        return "other/unknown"

    def _2d_arr_string(self, array, startspace=0):
        # return a string of the 2d array
        # first find maximal element length in each column
        maxlen = []
        for column in array.T:
            maxlencol = 0
            for element in column:
                length = len(str(element))
                if length > maxlencol:
                    maxlencol = length
            maxlen.append(maxlencol)
        # create string
        string = ""
        for row in array:
            string += " " * startspace + _SPACES  # beginning spaces
            str_row = []
            for i, element in enumerate(row):
                str_element = str(element)
                add_space = maxlen[i] - len(str_element) + 2
                str_row.append(" " * add_space + str_element)
            string += "".join(str_row) + "\n"
        # if last row, remove newline to not create unnecessary newline
        string = string[:-1]
        return string

    def __str__(self):
        # compute string representation of the variable
        string = f" {self.name}"
        # if value is a single string, int or float just append it
        if isinstance(self.value, str) or (isinstance(self.value, float) or
                                           isinstance(self.value, int)):
            return string + f" {str(self.value)}\n"
        if type(self.value) in (list, tuple, np.ndarray):
            # uniformize it
            self.value = np.array(self.value)
            if len(self.value.shape) == 1 or self.value.shape[0] == 1:
                if len(self.value.shape) > 1:
                    # 2D array with only one row => act as if it is a vector
                    self.value = self.value[0]
                return (string + _SPACES +
                        _SPACES.join([str(x) for x in self.value]) + "\n")
            # else, we are a 2d array
            if len(self.value.shape) == 2:
                startspace = len(string)
                return (string + "\n" +
                        self._2d_arr_string(self.value,
                                            startspace=startspace) +
                        "\n")
            else:
                raise TypeError(f"{self.name} don't have good dimentions.")
        else:
            raise TypeError(f"Unsupported data type for input file:"
                            f" {self.name}")


class OpticInputVariable(BaseInputVariable):
    """Class that represents an optic input variable.
    """
    _loggername = "OpticInputVariable"

    def __init__(self, *args, **kwargs):
        """Optic input variable init method.
        """
        super().__init__(*args, **kwargs)

    def _get_block(self):
        for block, variables in ALL_OPTIC_VARIABLES.items():
            if self.name in variables:
                return block
        raise NameError(f"Unrecognized OPTIC variable: {self.name}!")

    def __str__(self):
        string = f" {self.name} = "
        if isinstance(self.value, str):
            return string + "'" + self.value + "',\n"
        elif isinstance(self.value, float) or isinstance(self.value, int):
            return string + str(self.value) + ",\n"
        # else it is a list/tuple
        val = ",".join([str(x) for x in self.value])
        return string + val + ",\n"
