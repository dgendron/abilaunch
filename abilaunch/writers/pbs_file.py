from .bases import BaseWriter
from ..routines import check_abspath, return_list
from abilaunch import USER_CONFIG


class PBSFileWriter(BaseWriter):
    """Class that represents and write pbs files which serves to launch
    batch calculations on super clusters or simply on a PC."""
    _loggername = "PBSFileWriter"
    _file_suffix = ".sh"

    def __init__(self,
                 path,
                 files_file_path,
                 log_path="log",
                 stderr_path="stderr",
                 jobname=None,
                 walltime=None,
                 nodes=None,
                 ppn=None,
                 mpirun_command="",
                 abinit_command=None,
                 modules=None,
                 lines_before=None,
                 lines_after=None,
                 **kwargs):
        """PBS file writer init method.

        Parameters
        ----------
        path : str
               The path where the pbs file will be written.
        files_file_path : str
                          The path to the files file.
        log_path : str, optional
                   The path to the log file (default "log").
        stderr_path : str, optional
                      The path to the stderr output (default "stderr").
        jobname : str, optional
                  The jobname to be submitted. If None, no jobname is set.
        walltime : str, optional
                   The walltime needed for the job. e.g.: '01:00:00'
        nodes : str, int, optional
                The number of nodes needed for the job. e.g.: 1:m24G.
        ppn : int, optional
              The number of processors per nodes to be used by the batch job.
        mpirun_command : str, optional
                         The mpirun command to use for abinit to run in paral.
        abinit_command : str, optional
                         The path to the abinit executable.
        modules : list, optional
                  The list of modules to load if needed prior to the
                  calculation.
        lines_before : list, optional
                       Any command to run before the abinit script is
                       launched.
        lines_after : list, optional
                      Samething as lines_before but after the abinit script.
        """
        super().__init__(path, **kwargs)
        if not abinit_command:
            self._logger.info("Abinit executable not given -> take default.")
            abinit_command = USER_CONFIG.abinit_path
        self.files_file_path = check_abspath(files_file_path,
                                             join_before=self.workdir)
        self.log_path = check_abspath(log_path, join_before=self.workdir)
        self.stderr_path = check_abspath(stderr_path, join_before=self.workdir)
        self.jobname = jobname
        self.walltime = self._check_walltime(walltime)
        self.nodes = str(nodes) if nodes is not None else None
        self.ppn = str(ppn) if ppn is not None else None
        if ((self.nodes is not None and
             self.ppn is None) or (self.nodes is None and
                                   self.ppn is not None)):
            raise ValueError(f"nodes and ppn must not be None"
                             f" while the other is not None")
        self.mpirun_command = mpirun_command
        self.abinit_command = abinit_command
        self.modules = return_list(modules)
        self.lines_before = return_list(lines_before)
        self.lines_after = return_list(lines_after)

    def _check_walltime(self, walltime):
        if walltime is None:
            return walltime
        if len(walltime.split(":")) != 3:
            raise ValueError("Walltime must be formatted like"
                             " this: (hh:mm:ss).")
        return walltime

    def _do_write(self):
        lines = []
        # first line is bash line
        lines.append("#!/bin/bash\n\n")
        # next line is job name
        if self.jobname is not None:
            lines.append(f"#PBS -N {self.jobname}\n")
        # walltime
        if self.walltime is not None:
            lines.append(f"#PBS -l walltime={self.walltime}\n")
        # nodes/ppn
        if self.nodes is not None:
            lines.append(f"#PBS -l nodes={self.nodes}:ppn={self.ppn}\n")
        lines.append("\n")
        # ENV variables
        # mpirun
        lines.append(f'MPIRUN="{self.mpirun_command}"\n')
        # abinit executable
        lines.append(f"EXECUTABLE={self.abinit_command}\n")
        # files file
        lines.append(f"INPUT={self.files_file_path}\n")
        # log file
        lines.append(f"LOG={self.log_path}\n")
        # stderr file
        lines.append(f"STDERR={self.stderr_path}\n\n")
        # modules
        if self.modules:
            for module in self.modules:
                lines.append(f"module load {module}\n")
            lines.append("\n")
        # lines before
        if self.lines_before:
            for line in self.lines_before:
                if not line.endswith("\n"):
                    line += "\n"
                lines.append(line)
            lines.append("\n")
        # abinit command line
        lines.append("$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR\n")
        # lines after
        if self.lines_after:
            for line in self.lines_after:
                if not line.endswith("\n"):
                    line += "\n"
                lines.append(line)
            # don't need to add another blank space after
        self._write_lines(lines)
