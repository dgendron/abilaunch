from .bases import BaseApprover, BaseParalApprover
from .mpi_approver import MPIApprover
from numpy import prod


# there is utilities in abipy to validate input files but the only thing they
# do is to call abinit in dry run and check for errors. This approver class
# is more passive as it does not call abinit.

MANDATORY_VARS = ("ecut", "ntypat", "znucl", "typat", "acell")
TOLERANCES_VARS = ("toldfe", "tolwfr", "toldff", "tolrff")
ATOMIC_POSITION_VARS = ("xcart", "xred")
PARAL_VARS = ("npfft", "npkpt", "npband", "npspinor", "nphf")


class InputApprover(BaseApprover):
    """Class that checks if a set of input variable is
     valid for an abinit calculation. The 'valid' attribute states
     if the input should be good. If it is False, abinit will raise an error
     if it is launched with these variables.
    """
    _loggername = "InputApprover"

    def validate(self):
        super().validate()
        ndtset = self.variables.get("ndtset", 1)
        if ndtset > 1:
            self._logger.warning("Input approval is not implemented"
                                 " for multidtset.")
            return
        self._check_nscf_ok()
        self._check_basics()

    def _check_nscf_ok(self):
        iscf = self.variables.get("iscf", 0)
        if iscf < 0 and iscf != -3:
            # iscf < 0 and iscf != -3 => nscf calculation
            # check that tolwfr > 0
            tolwfr = self.variables.get("tolwfr", 0.0)
            if tolwfr <= 0.0:
                self.errors.append("for iscf < 0 and != -3, tolwfr"
                                   "  must be > 0.")

    def _check_basics(self):
        keys = list(self.variables.keys())
        check1, missings = self._all_in(MANDATORY_VARS, keys)
        check2, presents2 = self._only_one_in(TOLERANCES_VARS, keys)
        check3, presents3 = self._only_one_in(ATOMIC_POSITION_VARS, keys)
        if not check1:
            self.errors.append("%s should be in the input file!" %
                               str(missings))
        for check, presents in zip([check2, check3], [presents2, presents3]):
            if not check:
                self.errors.append("%s are presents in the input file but"
                                   " there should be only one from %s." %
                                   (str(presents2), str(TOLERANCES_VARS)))

    def _only_one_in(self, lst1, lst2):
        # check that there is exactly one item of list 1 in list 2
        presents = []
        result = True
        for item in lst1:
            if item in lst2:
                if len(presents):
                    result = False
                presents.append(item)
        return result, presents

    def _at_least_one_in(self, lst1, lst2):
        # check that at least one item of list 1 is in list 2
        for item in lst1:
            if item in lst2:
                return True
        return False

    def _all_in(self, lst1, lst2):
        # check that all variables in list 1 are in list 2
        missings = []
        result = True
        for item in lst1:
            if item not in lst2:
                result = False
                missings.append(item)
        return result, missings


class InputParalApprover(BaseParalApprover):
    """Class that compares the parallelization variables of Abinit with
    the mpi settings.
    """
    _loggername = "InputParalApprover"
    _input_approver = InputApprover
    _mpi_approver = MPIApprover

    def validate(self):
        super().validate()
        nodes = self.variables["nodes"]
        command = self.variables["mpirun_command"]
        self._logger.debug(f"Extracting mpirun_np from: {command}.")
        mpirun_np = MPIApprover.extract_npernode(command)
        if isinstance(nodes, str):
            if ":" in nodes:
                # nodes = 3:m48G  for example
                nodes = int(nodes.split(":")[0])
            else:
                nodes = int(nodes)
        self._logger.debug(f"Computing tot_ncpus from: nodes={nodes},"
                           f" mpirun_np={mpirun_np}")
        total_ncpus = nodes * mpirun_np
        paralvars = {var: int(self.variables.get(var, 1))
                     for var in PARAL_VARS}
        product = prod(list(paralvars.values()))
        if product > total_ncpus:
            self.errors.append(f"Parallelism scheme ask for too much proc:"
                               f" {paralvars} => {product}")
