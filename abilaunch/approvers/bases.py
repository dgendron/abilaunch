import abc
from ..bases import BaseUtility


class BaseApprover(BaseUtility):
    """Base class for approvers.
    """

    def __init__(self, variables, **kwargs):
        """Base approver init method.

        Parameters
        ----------
        variables : dict
                    The variables to approve.
        """
        super().__init__(**kwargs)
        self.variables = variables
        self.errors = []
        if not isinstance(self.variables, dict):
            raise TypeError("Variables should be a dict.")
        self.validate()

    @abc.abstractmethod
    def validate(self):
        """Abstrct method to validate variables.
        """
        self._logger.info("Validating variables.")

    @property
    def is_valid(self):
        """Returns True if variables are valid (i.e.: if no errors).
        """
        if len(self.errors):
            return False
        return True


class BaseParalApprover(BaseApprover):
    """Base class for input file approver and comparison with mpi settings.
    """
    _input_approver = None
    _mpi_approver = None

    def __init__(self, input_variables, mpi_variables, **kwargs):
        """Input paral approver. It takes all variables (mpi settings +
        input variables) as a single dictionary. If you have parameters
        separated, consider using MPIApprover and InputApprover separately
        and then call this class' classmethod cls.from_approvers.

        Parameters
        ----------
        variables : dict
                    The dictionary that contains both the mpi settings and the
                    abinit input variables.
        """
        newdict = input_variables.copy()
        for k, v in mpi_variables.items():
            if k in newdict:
                raise ValueError(f"{k} is in the abinit input variables...")
            newdict[k] = v
        super().__init__(newdict, **kwargs)

    @classmethod
    def from_approvers(cls, input_approver, mpi_approver, **kwargs):
        """Init method from an input file approver and a mpi approver.

        Parameters
        ----------
        input_approver : InputApprover instance.
        mpi_approver : MPIApprover instance.
        kwargs : other kwargs are passed to the constructor.
        """
        if not isinstance(input_approver, cls._input_approver):
            raise TypeError("First argument should be"
                            " an InputApprover instance.")
        if not isinstance(mpi_approver, cls._mpi_approver):
            raise TypeError("Second argument should be a MPIApprover"
                            " instance.")
        return cls(input_approver.variables, mpi_approver.variables, **kwargs)
