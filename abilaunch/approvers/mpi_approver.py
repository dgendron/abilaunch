from .bases import BaseApprover


class MPIApprover(BaseApprover):
    """Class that checks if mpi options are well set.
    """
    _loggername = "MPIApprover"

    def validate(self):
        super().validate()
        paral_params = self.variables
        ppn = paral_params["ppn"]
        mpirun_np = self.extract_npernode(paral_params["mpirun_command"])
        if mpirun_np is None:
            # No parallelization
            return
        if mpirun_np > ppn:
            self.errors.append(f"npernode {mpirun_np} call uses"
                               f" more proc than available"
                               f" on the nodes ({ppn})!")

    @staticmethod
    def extract_npernode(command):
        if command is None or not isinstance(command, str):
            return None
        # mpirun == 'mpirun -np 12' or
        # mpirun == 'mpirun -npernode 12'
        split = command.split()
        try:
            return int(split[-1])
        except ValueError:
            # just 'mpirun' => uses all avalaible proc
            return None
