from .bases import BaseApprover, BaseParalApprover
from .mpi_approver import MPIApprover


# TODO: implement this class
class OpticInputApprover(BaseApprover):
    """Optic input file approver (TODO: not implemented yet.)
    """
    _loggername = "OpticInputApprover"

    def validate(self):
        self._logger.warning("Optic input file approver not implemented yet.")


# TODO: implement this class also.
class OpticInputParalApprover(BaseParalApprover):
    """Class that compares the parallelization variables of optic with the
    mpi settings.
    """
    _loggername = "OpticParalApprover"
    _input_approver = OpticInputApprover
    _mpi_approver = MPIApprover

    def validate(self):
        self._logger.warning("Optic paral input file approver not"
                             " implemented yet.")
